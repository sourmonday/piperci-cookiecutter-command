#!/bin/bash

TEMPLATE_PATH=$1
TEMPLATE_INSTALL_PATH=$2
THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")
THIS_RELDIR=$(dirname "${BASH_SOURCE[0]}")
REPO_BASEDIR=$(cd $THIS_RELDIR; cd ../../; pwd)

cd ${REPO_BASEDIR}

[[ -d ${TEMPLATE_INSTALL_PATH} ]] || exit 1

if ! [[ -d ${TEMPLATE_INSTALL_PATH}/template ]];
then
  cd ${TEMPLATE_INSTALL_PATH}
  curl -o template.tar.gz ${TEMPLATE_PATH}
  tar -xzf template.tar.gz --strip-components=1
else
  echo "template already installed for this env skipping"
fi
