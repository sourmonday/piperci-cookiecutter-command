import pytest

from piperci_{{cookiecutter.command_name}}_executor.function.faas_app import app as papp
from piperci_{{cookiecutter.command_name}}_executor.function.config import Config


@pytest.fixture
def app():  # required by pytest_flask
    return papp


@pytest.fixture
def config():
    return Config
