import pytest
import responses

from piperci.faas.exceptions import PiperError
from piperci.storeman.client import storage_client
from piperci_echo_executor.function.handler import execute


def test_gateway_no_json(client):
    headers = {"Content-Type": "application/json"}
    resp = client.post("test", headers=headers)
    assert resp.status_code == 422


def test_gateway_no_content_type_json(client, single_instance):
    resp = client.post("test", data=single_instance)
    assert resp.status_code == 422


@pytest.mark.parametrize(
    "invalid_command_replacement",
    ["invalid-cmd", ["invalid-cmd arg1", "invalid-cmd arg2"]],
)
def test_execute_function_error(mocker, invalid_command_replacement):
    mock_task = mocker.patch("piperci.faas.this_task.ThisTask.info")
    mock_request = mocker.patch("flask.Request", autospec=True)
    mock_request.get_json.return_value = {"run-cmd": invalid_command_replacement}

    with pytest.raises(PiperError):
        execute(mock_request, mock_task, "path/to/artifact")


@responses.activate
@pytest.mark.parametrize(
    "command, task_artifact_patch_flag",
    [
        ("failed-cmd arg2", False),
        ("failed-cmd arg2", True),
        (["not-valid-1", "not-valid-2"], False),
        ({"key": "this-is not a valid type"}, False),
    ],
)
def test_executor_handler_error_handler(
    client,
    threaded_instance,
    command: str,
    common_headers: dict,
    start_task_response,
    task_creation_response,
    task_artifact_patch_flag: bool,
    task_artifact_upload_response,
    mocker,
    minio_server,
):
    invalid_instance = threaded_instance
    invalid_instance["run-cmd"] = command

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.faas.this_task.ThisTask._init_storage_client", store_cli)
    if task_artifact_patch_flag:
        mocker.patch(
            "piperci_{{cookiecutter.command_name}}_executor.function.handler"
            + ".validate"
        )
        mocker.patch("piperci.faas.this_task.ThisTask.artifact", side_effect=PiperError)

    resp = client.post("invalid-run", json=invalid_instance, headers=common_headers)
    assert resp.status_code == 400


@responses.activate
def test_executor_client_task_info_fail(
    client, common_headers, mocker, threaded_instance, start_task_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    mock_execute = mocker.patch(
        "piperci_{{cookiecutter.command_name}}_executor." + "function.handler.execute"
    )
    mocker.patch(
        "piperci_{{cookiecutter.command_name}}_gateway.function.handler" + ".validate"
    )

    resp = client.post("test", json=threaded_instance, headers=common_headers)
    assert resp.status_code == 400
    mock_execute.assert_not_called()
